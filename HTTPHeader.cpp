#include <HTTPHeader.h>
#include <sstream>
#include <vector>

namespace HTML
{

    HTTP_HEADER::HTTP_HEADER(const std::string &key, const std::string &value) noexcept: key(key), value(value)
    {
        
    }
    
    void HTTP_HEADER::set_value(const std::string &value) noexcept
    {
        this->value = value;
    }
    
    const std::string HTTP_HEADER::get_key() const noexcept
    {
        return this->key;
    }
    
    std::string HTTP_HEADER::serialize() const noexcept
    {
        std::string header;
        header += this->key;
        header += ": ";
        header += this->value;
        header += HTTP_HEADER_LINE_END;
        return header;
    }
    
    HTTP_HEADER HTTP_HEADER::deserialize(const std::string &header)
    {
        std::vector<std::string> segments;
        std::stringstream strStream(header);
        std::string strToken;
        while(std::getline(strStream,strToken,' '))
            segments.push_back(strToken);
        
        const std::string key = segments[0].substr(0, segments[0].size() -1);
        const size_t value_end = segments[1].find('\r');
        const std::string value = segments[1].substr(0,value_end);
        return  HTTP_HEADER(key,value);

    }
        
}

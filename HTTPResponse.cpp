#include <HTTPResponse.h>
#include <iostream>


namespace HTML
{
    
    HTTP_RESPONSE::HTTP_RESPONSE(const Generic::HTTP_RESPONSE_CODE response_code, 
                        const Generic::HTTP_VERSION http_version,
                        const std::string& body,
                        std::map<std::string,HTTP_HEADER> &headers
                        ) noexcept :
                        http_version(http_version),code(response_code), 
                        body(body),headers(headers) {}
    
    HTTP_RESPONSE::HTTP_RESPONSE(const HTTP_RESPONSE &other) noexcept
    {
        this->http_version = other.get_http_version();
        this->code = other.get_response_code();
        this->body = other.get_body();
        this->headers = other.get_headers();
    }
 
    Generic::HTTP_VERSION HTTP_RESPONSE::get_http_version() const noexcept
    {
        return this->http_version;
    }
    
    Generic::HTTP_RESPONSE_CODE HTTP_RESPONSE::get_response_code() const noexcept
    {
        return this->code;
    }
    
    const std::string& HTTP_RESPONSE::get_body() const noexcept
    {
        return this->body;
    }
    
    std::map<std::string,HTTP_HEADER> HTTP_RESPONSE::get_headers() const noexcept
    {
        return this->headers;
    }
    
    std::string HTTP_RESPONSE::serialize() const noexcept
    {
        std::string response_message;
        response_message = (uint8_t)code;
        response_message += " ";
        response_message += Generic::http_version_to_string(http_version);
        response_message += HTTP_HEADER_LINE_END;
        
        std::map<std::string,HTTP_HEADER>::const_iterator header_pointer = 
                    headers.begin();
                                
        while(header_pointer != headers.end())
        {
            response_message += (*header_pointer).second.serialize();
            header_pointer++;
        }
        
        response_message += std::string(HTTP_HEADER_LINE_END) + HTTP_HEADER_LINE_END;
        
        response_message += body;
        
        return response_message;
    }
    
    HTTP_RESPONSE HTTP_RESPONSE::deserialize(const std::string& response) noexcept
    {
        std::vector<std::string> segments;
        size_t string_start = 0, string_end = 0;
        std::string delimiter = std::string(HTTP_HEADER_LINE_END) + HTTP_HEADER_LINE_END;
        while((string_end = response.find(delimiter,string_end)) != std::string::npos)
        {
            segments.push_back(response.substr(string_start,(string_end - string_start)));
            string_end += delimiter.length();
            string_start = string_end;
        }
        
        std::string header = segments[0];
        std::string body = segments[1];
        segments.clear();
        delimiter = HTTP_HEADER_LINE_END;
        string_start = string_end = 0;
        while((string_end = header.find(delimiter,string_end)) != std::string::npos)
        {
            segments.push_back(header.substr(string_start,(string_end - string_start)));
            string_end += delimiter.length();
            string_start = string_end;
        }
        
        const std::string base_header_line = segments[0];
        std::map<std::string,HTTP_HEADER> http_headers;
        std::vector<std::string>::iterator header_line_pointer = std::next(segments.begin());
        while(header_line_pointer != segments.end())
        {
            const HTTP_HEADER header = HTTP_HEADER::deserialize(*header_line_pointer);
            http_headers.insert(std::make_pair(header.get_key(),header));
            header_line_pointer++;
        }
        segments.clear();
        
        std::stringstream base_header_stream(base_header_line);
        std::string required_segment;
        while(std::getline(base_header_stream,required_segment,' '))
            segments.push_back(required_segment);
        
        if(segments.size() != 2)
        {
            std::cerr << "Failed to parse HTTP response header required info !. Aborting operation !" << std::endl;
            //throw std::runtime_error("Failed to parse HTTP response header required info !. Aborting operation !");
        }
            
        
        Generic::HTTP_RESPONSE_CODE response_code = (Generic::HTTP_RESPONSE_CODE)atoi(segments[0].c_str());                          
        Generic::HTTP_VERSION http_version = Generic::http_version_from_string(segments[1]);
        return HTTP_RESPONSE(response_code,http_version,body,http_headers);     
    }
    
}

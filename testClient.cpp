
/***************************** HTTP Client V1.0 *********************************************
 * Developed by Suran Mahawithana 2020-03-29
 * This is a simiplest HTTP client implementation one could ever imagine
 * this client connects with the simple html server and does some sending and receiving of data
 * to and from the server
 * ******************************************************************************************
 */

#include <iostream> 
#include <sys/socket.h>
#include <arpa/inet.h>
#include <string.h>
#include <unistd.h>

struct sockaddr_in serv_address;
#define SERVER_PORT 8500

int main() {
    
    int sock = 0; 
    long valRead = 0;
    char clientMsg[] = "Hello from client !";
    char buffer [1024] = {0};
    if((sock = socket(AF_INET,SOCK_STREAM,0)) < 0)
    {
        std::cerr << "Failed to create socket !" <<std::endl;
        exit(EXIT_FAILURE);
    }
    
    memset(&serv_address,'0',sizeof(serv_address));
    serv_address.sin_family = AF_INET;
    serv_address.sin_port = htons(SERVER_PORT);
    //or this could be used to set server ip
    //erv_address.sin_addr.s_addr = inet_addr(char *serverIp);
    
    if(inet_pton(AF_INET,"127.0.0.1",&serv_address.sin_addr) <= 0)
    {
        std::cerr << "Invalid address !. Address not supported !" <<std::endl;
        exit(EXIT_FAILURE);
    }

    if(connect(sock,(struct sockaddr *)&serv_address,sizeof(serv_address)) < 0)
    {
        std::cerr << "Connection failed to the server !" << std::endl;
        exit(EXIT_FAILURE);
    }
    
    send(sock,clientMsg,strlen(clientMsg),0);
    std::cout << "Message sent to the server !" << std::endl;
    valRead = recv(sock,buffer,1024,0);
    if(valRead > 0)
        std::cout << "Message from Server : " << buffer << std::endl;
    else
        std::cerr << "Failed to receive bytes from the server !" << std::endl;
    
    close(sock);
    return 0;
}


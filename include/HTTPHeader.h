#ifndef HTTP_HEADER_H
#define HTTP_HEADER_H

#include <string>

namespace HTML
{
    constexpr static char HTTP_HEADER_LINE_END[] = "\r\n";
        
    class HTTP_HEADER
    {
    private:
        std::string key;
        std::string value;
        
    public:
        HTTP_HEADER(const std::string &key, const std::string &value) noexcept;
        void set_value(const std::string &value)noexcept;
        const std::string get_key() const noexcept;
        std::string serialize() const noexcept;
        static HTTP_HEADER deserialize(const std::string &header);
        
    };
    
}

#endif

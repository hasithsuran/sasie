#ifndef HTML_GENERIC_H
#define HTML_GENERIC_H

#include <string>

namespace HTML {

class Generic
{
  
public:
    enum class HTTP_VERSION : uint8_t
    {
        UNDEFINED   = 0x00,
        HTTP_1_0    = 0x01,
        HTTP_1_1    = 0x02,
        HTTP_2_0    = 0x03
    };
    
    enum class HTTP_REQUEST_METHOD : uint8_t
    {
        UNDEFINED   = 0x00,
        GET         = 0x01,
        POST        = 0x02,
        PUT         = 0x03,
        DELETE      = 0x04,
        HEAD        = 0x05,
        PATCH       = 0x06,
        OPTIONS     = 0x07
        
    };
    
    enum class HTTP_RESPONSE_CODE : uint16_t
    {
        OK                      = 200,
        CREATED                 = 201,
        ACCEPTED                = 202,
        NO_CONTENT              = 203,
        BAD_REQUEST             = 400,
        NOT_FOUND               = 404,
        REQUEST_TIMEOUT         = 408,
        INTERNAL_SERVER_ERROR   = 500,
        BAD_GATEWAY             = 502,
        SERVICE_UNAVAILABLE     = 503         
    };
    
    static std::string request_method_to_string(HTTP_REQUEST_METHOD method);
    static HTTP_REQUEST_METHOD request_method_from_string(std::string &method);
    static std::string http_version_to_string(HTTP_VERSION version);
    static HTTP_VERSION http_version_from_string(std::string &version);
};

}

#endif

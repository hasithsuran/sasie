#ifndef NET_SOCK_H
#define NET_SOCK_H

#include <cinttypes>
#include <string>
#include <sys/socket.h>
#include <arpa/inet.h>

namespace SystemCore
{
    class SasieSocket
    {
    protected:
        int socket_id;
        struct sockaddr_in *address;
        socklen_t address_size;
        bool isValidSocket;
        bool connected;
        
        inline void setConnectionState(bool state) noexcept;
        
    public:
        SasieSocket();
        SasieSocket(int socket_id,struct sockaddr_in *address, socklen_t address_size) noexcept;
        virtual ~SasieSocket();
        virtual bool isValid() const noexcept = 0;
        virtual bool isConnected() const noexcept = 0;
        virtual void close_socket() = 0;
    };
    
    class SasieDataSocket;
    
    class SasieListeningSocket : virtual public SasieSocket
    {
    private:
        char* listening_address;
        uint16_t listening_port;
        uint16_t recieve_queue_size;
    public:
        SasieListeningSocket(const uint16_t max_queue_size) noexcept;
        virtual ~SasieListeningSocket() noexcept;
        
        bool bind_to(const char* in_address,uint16_t in_port);
        bool makeListenable();
        SasieDataSocket* get_client_connection() const;
        
        char* get_listening_address() const noexcept;
        uint16_t get_listening_port() const noexcept;
        
        virtual bool isValid() const noexcept override;
        virtual bool isConnected() const noexcept override;
        virtual void close_socket() override;
    };
    
    class SasieDataSocket : virtual public SasieSocket
    {
    private:
        size_t bytes_sent;
        size_t bytes_received;
        char* remote_address;
        uint16_t remote_port;
        SasieDataSocket(int socket_id,struct sockaddr_in *address, socklen_t address_size) noexcept;
    public:
        SasieDataSocket() noexcept;
        virtual ~SasieDataSocket() noexcept;
        
        friend  SasieDataSocket* SasieListeningSocket::get_client_connection() const;
        
        bool connect_to(const char* remote_address,uint16_t remote_port);
        
        bool sendStringData(const std::string &data);
        bool send_cstring_ata(const char* data,size_t data_byte_size);
        bool send_raw_buffer_data(const void* data_buffer,size_t buffer_byte_size);
        
        size_t receive_data_to_string(std::string &buffer,const size_t max_data_byte_size);
        size_t receive_data_to_cstring(char *string_buffer,const size_t max_data_byte_size);
        size_t receive_data_to_buffer(void* data_buffer,const size_t max_data_byte_size);
        
        size_t get_bytes_sent() const noexcept;
        size_t get_bytes_received() const noexcept; 
        
        char* get_remote_address() const noexcept;
        uint16_t get_remote_port() const noexcept;
        
        virtual bool isValid() const noexcept override;
        virtual bool isConnected() const noexcept override;
        virtual void close_socket() override;
        
    };
    
}

#endif

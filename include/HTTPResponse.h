#ifndef HTTP_RESPONSE_H
#define HTTP_RESPONSE_H

#include <Generic.h>
#include <HTTPHeader.h>
#include <map>
#include <vector>
#include <cstring>
#include <sstream>

namespace HTML
{
    class HTTP_RESPONSE
    {
    private:
        Generic::HTTP_VERSION http_version;
        Generic::HTTP_RESPONSE_CODE code;
        std::string body;
        std::map<std::string,HTTP_HEADER> headers;
        
    public:
        HTTP_RESPONSE(const Generic::HTTP_RESPONSE_CODE response_code, 
                                const Generic::HTTP_VERSION http_version,
                                const std::string& body,
                                std::map<std::string,HTTP_HEADER> &headers
                                ) noexcept;
                                
        HTTP_RESPONSE(const HTML::HTTP_RESPONSE& other) noexcept;
        
        Generic::HTTP_VERSION get_http_version() const noexcept;
        Generic::HTTP_RESPONSE_CODE get_response_code() const noexcept;        
        const std::string& get_body() const noexcept;
        std::map<std::string,HTTP_HEADER> get_headers() const noexcept;        
        std::string serialize() const noexcept;
        static HTTP_RESPONSE deserialize(const std::string& response) noexcept;
        
    };
    
}

#endif

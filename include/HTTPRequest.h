#ifndef HTTP_REQUEST_H
#define HTTP_REQUEST_H

#include <Generic.h>
#include <HTTPHeader.h>
#include <cstring>
#include <map>
#include <vector>
#include <sstream>

namespace HTML
{
    
    class HTTP_REQUEST
    {
    private:
        Generic::HTTP_REQUEST_METHOD request_method;
        Generic::HTTP_VERSION http_version;
        std::string resource;
        std::map<std::string,HTTP_HEADER> headers;
    
    public:
        
        HTTP_REQUEST(Generic::HTTP_REQUEST_METHOD method, const std::string &resource,
                     std::map<std::string,HTTP_HEADER> &headers, 
                     Generic::HTTP_VERSION version = Generic::HTTP_VERSION::HTTP_1_0) noexcept;
        
        std::string serialize() const noexcept;
        static HTTP_REQUEST desirialize(const std::string &request);
        
    };
    
}

#endif

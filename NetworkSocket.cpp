#include <NetworkSocket.h>
#include <iostream>
#include <string.h>
#include <unistd.h>

namespace SystemCore
{
    static const uint16_t IP4_NET_ADDR_STR_SIZE = 16;
    SasieSocket::SasieSocket()
    {
        if((socket_id = socket(AF_INET,SOCK_STREAM,0)) < 0)
        {
            std::cerr <<"Could not create socket !" << std::endl;
            exit(EXIT_FAILURE);
        }
        address = new struct sockaddr_in;
        memset((char*)address,0,sizeof(*address));
        setConnectionState(false);
    }

    SasieSocket::SasieSocket(int socket_id, struct sockaddr_in* address, socklen_t address_size) noexcept :
    socket_id(socket_id), address(address), address_size(address_size) 
    {
        setConnectionState(true);
    }
    
    void SasieSocket::setConnectionState(bool state) noexcept
    {
        this->connected = state;
    }
    
    SasieSocket::~SasieSocket()
    {
        delete address;
    }

    SasieDataSocket::SasieDataSocket() noexcept : SasieSocket() 
    {
        remote_address = new char[IP4_NET_ADDR_STR_SIZE];
        bytes_sent = bytes_received = 0;
    }
    
    SasieDataSocket::~SasieDataSocket() noexcept
    {
        delete[] remote_address;
    }
    
    SasieDataSocket::SasieDataSocket(int in_socket_id, struct sockaddr_in* in_address, socklen_t in_address_size) noexcept:
    SasieSocket(in_socket_id,in_address,in_address_size) 
    {
        remote_address = new char[IP4_NET_ADDR_STR_SIZE];
        strncpy(remote_address,inet_ntoa(in_address->sin_addr),IP4_NET_ADDR_STR_SIZE);
        remote_port = ntohs(in_address->sin_port);
        bytes_sent = bytes_received = 0;
    }
    
    bool SasieDataSocket::connect_to(const char* rmt_address,uint16_t rmt_port)
    {
        if(!isConnected() && (address != nullptr))
        {
            
            address->sin_family = AF_INET;
            if(inet_pton(AF_INET,rmt_address,&(address->sin_addr)) <= 0)
            {
                std::cerr << "Invalid IP address !. Address not supported !" <<std::endl;
                memset((char*)address,0,sizeof(struct sockaddr_in));
                return false;
            }
            
            address->sin_port = htons(rmt_port);
            
            if(connect(socket_id,(struct sockaddr *)(address),sizeof(address)) < 0)
            {
                std::cerr << "Connection failed to the server !" << std::endl;
                return false;
            }
            strncpy(remote_address,rmt_address,IP4_NET_ADDR_STR_SIZE);
            remote_port = rmt_port;
        
            setConnectionState(true);
            return true;
        }
        
        return false;
    }
        
    bool SasieDataSocket::sendStringData(const std::string &data)
    {
        uint32_t sent_byte_count;
        sent_byte_count = send(socket_id,data.c_str(),data.length(),0);
        bytes_sent += sent_byte_count;
        return ( sent_byte_count == data.length() );
        
    }
    bool SasieDataSocket::send_cstring_ata(const char* data,size_t data_byte_size)
    {
        uint32_t sent_byte_count;
        sent_byte_count = send(socket_id,data,data_byte_size,0);
        bytes_sent += sent_byte_count;
        return ( sent_byte_count == data_byte_size);
    }
    
    bool SasieDataSocket::send_raw_buffer_data(const void* data_buffer,size_t buffer_byte_size)
    {
        uint32_t sent_byte_count;
        sent_byte_count = send(socket_id,static_cast<const char*>(data_buffer),buffer_byte_size,0);
        bytes_sent += sent_byte_count;
        return ( sent_byte_count == buffer_byte_size );
    }
    
    size_t SasieDataSocket::receive_data_to_string(std::string &string_buffer,const size_t max_data_byte_size)
    {
        char temp_buffer[max_data_byte_size];
        size_t read_byte_count = receive_data_to_cstring(temp_buffer,max_data_byte_size);
        string_buffer = temp_buffer;
        return read_byte_count;
        
    }
    size_t SasieDataSocket::receive_data_to_cstring(char *char_buffer,const size_t max_data_byte_size)
    {
        size_t read_byte_count = recv(socket_id,char_buffer,max_data_byte_size,0);
        bytes_received += read_byte_count;
        return read_byte_count;
    }
    size_t SasieDataSocket::receive_data_to_buffer(void* data_buffer,const size_t max_data_byte_size)
    {
        return receive_data_to_cstring(static_cast<char *>(data_buffer),max_data_byte_size);
    }
    
    size_t SasieDataSocket::get_bytes_sent() const noexcept
    {
        return bytes_sent;
    }
    size_t SasieDataSocket::get_bytes_received() const noexcept
    {
        return bytes_received;
    }
    
    char* SasieDataSocket::get_remote_address() const noexcept
    {
        return remote_address;
    }
    
    uint16_t SasieDataSocket::get_remote_port() const noexcept
    {
        return remote_port;
    }
    
    bool SasieDataSocket::isValid() const noexcept
    {
        return isValidSocket;
    }
    
    bool SasieDataSocket::isConnected() const noexcept
    {
        return connected;
    }
    
    void SasieDataSocket::close_socket() 
    {
        close(socket_id);
    }
    
    
    SasieListeningSocket::SasieListeningSocket(const uint16_t max_queue_size) noexcept : SasieSocket() 
    {
            listening_address = new char[IP4_NET_ADDR_STR_SIZE];
            recieve_queue_size = max_queue_size;
    }
    
    SasieListeningSocket::~SasieListeningSocket() noexcept 
    {
        delete[] listening_address;
    }
    
    bool SasieListeningSocket::bind_to(const char* in_address,uint16_t in_port)
    {
        address->sin_family = AF_INET;
        
        if(strcmp(in_address,"0,0,0,0") == 0)
            address->sin_addr.s_addr = htonl(INADDR_ANY);
        else if(inet_pton(AF_INET,in_address,&(address->sin_addr)) <= 0)
        {
            std::cerr << "Invalid IP address !. Address not supported !" <<std::endl;
            memset((char*)address,0,sizeof(struct sockaddr_in));
            return false;
        }
        address->sin_port = htons(in_port);

        if(bind(socket_id,(struct sockaddr *)address,sizeof(*address)) < 0)
        {
            std::cerr <<"Socket binding failed !" << std::endl;
            memset((char*)address,0,sizeof(struct sockaddr_in));
            return false;
        }
        strncpy(listening_address,in_address,IP4_NET_ADDR_STR_SIZE);
        listening_port = in_port;
        
        return true;
    }
    
    bool SasieListeningSocket::makeListenable()
    {
        if(listen(socket_id, recieve_queue_size) < 0)
        {
            std::cerr << "Failed to listen on socket !" << std::endl;
            return false;
        }
        setConnectionState(true);
        return true;
    }
    SasieDataSocket* SasieListeningSocket::get_client_connection() const
    {
        struct sockaddr_in* client_address = new struct sockaddr_in;
        int client_sock_id;
        socklen_t address_length;
        
        if((client_sock_id = accept(socket_id,(struct sockaddr *)client_address,&address_length)) < 0)
        {
            std::cerr << "Failed to accept connection to client socket !" << std::endl;
            perror("my error ! :"); 
            delete client_address;
            return NULL;
        }
        SasieDataSocket* clientSocket = new SasieDataSocket(client_sock_id,client_address,address_length);
        return clientSocket;
    }
    
    char* SasieListeningSocket::get_listening_address() const noexcept
    {
        return listening_address;
    }
    
    uint16_t SasieListeningSocket::get_listening_port() const noexcept
    {
        return listening_port;
    }
    
    bool SasieListeningSocket::isValid() const noexcept
    {
        return isValidSocket;
    }
    bool SasieListeningSocket::isConnected() const noexcept
    {
        return connected;
    }
    void SasieListeningSocket::close_socket()
    {
        close(socket_id);
    }
    
}

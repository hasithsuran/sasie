
/******************** SASIE - Suran's Advanced Server Infrastructure Extensions V1.0 ***************** 
* This http server was written with performance optimization and extension in mind. 
* 
******************************************************************************************************/

#include <iostream>
#include <string.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/fcntl.h>

#include <NetworkSocket.h>
#include <sys/socket.h>
#include <arpa//inet.h>

#define PORT_NUMBER 8500
#define RECEIVE_BUFFER_SZIE 1024
#define MAX_QUEUED_CONNECTIONS 5
#define MAX_HTTP_PAGE_BYTE_SIZE 1000000


void handleClientSocket(SystemCore::SasieDataSocket *client);
const char* createMemoryMapForFile(const char* file_name,size_t& length,int &fild_desc);

int main() {
    
    SystemCore::SasieListeningSocket listner_socket(MAX_QUEUED_CONNECTIONS);
     
    listner_socket.bind_to("0.0.0.0",PORT_NUMBER);
    listner_socket.makeListenable();
    
    while(true)
    {
        
    std::cout << " ============= server listening on port " << PORT_NUMBER << " ! =========================== " << std::endl;
    SystemCore::SasieDataSocket* client_socket = listner_socket.get_client_connection();
    
    std::cout <<"Handling client : " << client_socket->get_remote_address() << std::endl;
    handleClientSocket(client_socket);
    
    }
    
    listner_socket.close_socket();
    std::cout << "Server going down. Bye !" << std::endl;
    return 0;
}

void handleClientSocket(SystemCore::SasieDataSocket *client)
{
    std::string buffer;
    long bytes_read = client->receive_data_to_string(buffer,RECEIVE_BUFFER_SZIE);
     
    std::cout << bytes_read << " bytes read from client !" << std::endl;
    std::cout << "Message read from client : \"" << buffer << "\"" << std::endl;
    
    size_t file_byte_size;
    int html_file_desc;
    const char* file_block_start = 
    createMemoryMapForFile("public_html/index.html",file_byte_size,html_file_desc);
    
    const char *htmlHeader = "HTTP/1.1 200 OK\nContent-Type: text/html\nContent-Length: ";
    char replyMsg[strlen(htmlHeader) + file_byte_size + 1];
    strcpy(replyMsg,htmlHeader);
    
    if(file_byte_size <= MAX_HTTP_PAGE_BYTE_SIZE)
    {
        char content_length[10];
        sprintf(content_length,"%zu\n\n",file_byte_size);
        strncat(replyMsg,content_length, strlen(content_length));
        strncat(replyMsg, file_block_start, file_byte_size);
    }
    else
    {
        char content_length[10];
        const char* clientWarnMsg = "Reached maximum size limit of HTML file !";
        sprintf(content_length,"%zu\n\n",strlen(clientWarnMsg));
        strncat(replyMsg,content_length, strlen(content_length));
        strncat(replyMsg,clientWarnMsg,strlen(clientWarnMsg));
    }

    
    if(munmap((void *)file_block_start,file_byte_size) != 0)
    {
        std::cerr << "could not unmap allocated file block on memory !" << std::endl;
        exit(EXIT_FAILURE);
    }
    
    close(html_file_desc);
    std::string client_reply = replyMsg;
    
    if(client->sendStringData(client_reply))
        std::cout << "Reply message sent to client !" << std::endl;
    else
        std::cerr << "Failed to send reply message to client !" << std::endl;
    
    client->close_socket();
    delete client;
    
}

const char* createMemoryMapForFile(const char* file_name,size_t& length,int &fild_desc)
{
    fild_desc = open(file_name,O_RDONLY);
    if(fild_desc == -1)
    {
        std::cerr << "Failed to open http file !" << std::endl;
        exit(EXIT_FAILURE);
    }
    
    struct stat st;
    if(fstat(fild_desc,&st) == -1)
    {
        std::cerr <<"Failed to retrieve file status !" << std::endl;
        exit(EXIT_FAILURE);
    }
    
    length = st.st_size;
    const char* mem_block_start = 
    static_cast<const char*>(mmap(NULL,length,PROT_READ,MAP_PRIVATE,fild_desc,0u));
    if(mem_block_start == MAP_FAILED)
    {
        std::cerr << "Failed to map file to the memory block !" << std::endl;
        exit(EXIT_FAILURE);
        
    }
    
    return mem_block_start;
        
}

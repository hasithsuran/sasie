#include <Generic.h>

namespace HTML 
{

    std::string Generic::request_method_to_string(HTTP_REQUEST_METHOD method)
    {
        switch(method)
        {
            case HTTP_REQUEST_METHOD::GET:          return "GET";
            case HTTP_REQUEST_METHOD::POST:         return "POST";
            case HTTP_REQUEST_METHOD::PUT:          return "PUT";
            case HTTP_REQUEST_METHOD::DELETE:       return "DELETE";
            case HTTP_REQUEST_METHOD::HEAD:         return "HEAD";
            case HTTP_REQUEST_METHOD::PATCH:        return "PATCH";
            case HTTP_REQUEST_METHOD::OPTIONS:      return "OPTIONS";
            case HTTP_REQUEST_METHOD::UNDEFINED:    return "UNDEFINED";
            default:                                return "";
            
        }
        
    }
    
    Generic::HTTP_REQUEST_METHOD Generic::request_method_from_string(std::string& method)
    {
        if(method == request_method_to_string(HTTP_REQUEST_METHOD::GET))
            return HTTP_REQUEST_METHOD::GET;
        else if(method == request_method_to_string(HTTP_REQUEST_METHOD::POST))
            return HTTP_REQUEST_METHOD::POST;
        else if(method == request_method_to_string(HTTP_REQUEST_METHOD::PUT))
            return HTTP_REQUEST_METHOD::PUT;
        else if(method == request_method_to_string(HTTP_REQUEST_METHOD::DELETE))
            return HTTP_REQUEST_METHOD::DELETE;
        else if(method == request_method_to_string(HTTP_REQUEST_METHOD::HEAD))
            return HTTP_REQUEST_METHOD::HEAD;
        else if(method == request_method_to_string(HTTP_REQUEST_METHOD::PATCH))
            return HTTP_REQUEST_METHOD::PATCH;
        else if(method == request_method_to_string(HTTP_REQUEST_METHOD::UNDEFINED))
            return HTTP_REQUEST_METHOD::UNDEFINED;
        return HTTP_REQUEST_METHOD::UNDEFINED;
        
    }
    
    std::string Generic::http_version_to_string(Generic::HTTP_VERSION version)
    {
        switch(version)
        {
            case HTTP_VERSION::HTTP_1_0:    return "HTTP/1.0";
            case HTTP_VERSION::HTTP_1_1:    return "HTTP/1.1";
            case HTTP_VERSION::HTTP_2_0:    return "HTTP/2.0";
            case HTTP_VERSION::UNDEFINED:   return "undefined";
            default:                        return "";
        }
        
    }
    
    Generic::HTTP_VERSION Generic::http_version_from_string(std::string &version)
    {
        if(version == http_version_to_string(HTTP_VERSION::HTTP_1_0)) return HTTP_VERSION::HTTP_1_0;
        else if(version == http_version_to_string(HTTP_VERSION::HTTP_1_1)) return HTTP_VERSION::HTTP_1_1;
        else if(version == http_version_to_string(HTTP_VERSION::HTTP_2_0)) return HTTP_VERSION::HTTP_2_0;
        else if(version == http_version_to_string(HTTP_VERSION::UNDEFINED)) return HTTP_VERSION::UNDEFINED;
        else return HTTP_VERSION::UNDEFINED;
        return HTTP_VERSION::UNDEFINED;
    }
    
}

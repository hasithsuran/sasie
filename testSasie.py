
############################ NOTE #################################################
# This simple script generates specified number of http request to the 
# our SASIE server randomly choosing visitable URI's. This script was built with 
# the purpose of stressing our server and pinpointing where possible performance 
# bottlenecks could happen. 
##################################################################################33  
#pip install requests prior to running this script

import requests
import time
import datetime
from random import seed
from random import random
from random import randint

server_address="http://localhost:8500"
num_requests = 50
visitable_pages = ["index.html","small_page.html","large_page.html"]
seed(500)

for x in range(num_requests):
	start_time = datetime.datetime.now()
	address_to_visit = server_address + "/" + visitable_pages[randint(0, len(visitable_pages) - 1)]
	print "Request # ", (x+1)
	print "Visiting URL : ", address_to_visit
	httpResponse =requests.get(address_to_visit)
	end_time = datetime.datetime.now()
	elapsed = end_time - start_time
	http_response = httpResponse.status_code
	if http_response == 200:
		print("Response - OK")
		print "Response time [ seconds ",elapsed.seconds , ", microseconds ", elapsed.microseconds , " ]"
		print ""
	else:
		print("Response failed")
	print "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"



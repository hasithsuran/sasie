#include <HTTPRequest.h>

namespace HTML
{
        HTTP_REQUEST::HTTP_REQUEST(Generic::HTTP_REQUEST_METHOD method, const std::string &resource,
                    std::map<std::string,HTTP_HEADER> &headers, Generic::HTTP_VERSION version) 
        noexcept : request_method(method),http_version(version),resource(resource),headers(headers)
        {
            
        }
        
        std::string HTTP_REQUEST::serialize() const noexcept
        {
            std::string request;
            request += Generic::request_method_to_string(this->request_method);
            request += " ";
            request += this->resource;
            request +=  " ";
            request += Generic::http_version_to_string(this->http_version);
            request += HTTP_HEADER_LINE_END;
            
            std::map<std::string,HTTP_HEADER>::const_iterator header_pointer = 
                                    this->headers.begin();
                                    
            while(header_pointer != this->headers.end())
            {
                request += (*header_pointer).second.serialize();
                header_pointer++;
            }
                                
            
            return request;
        }
        
        HTTP_REQUEST HTTP_REQUEST::desirialize(const std::string &request)
        {
            std::vector<std::string> request_header_lines;
            size_t string_start = 0, string_end = 0;
            while((string_end = request.find(HTTP_HEADER_LINE_END,string_end)) != std::string::npos)
            {
                request_header_lines.push_back(request.substr(string_start,(string_end - string_start)));
                string_start += std::strlen(HTTP_HEADER_LINE_END);
                string_end += std::strlen(HTTP_HEADER_LINE_END);
            }
            
            if(request_header_lines.size() < 1)
                throw std::runtime_error("Mal formed HTTP request failed to proceed !");
            
            std::vector<std::string> http_required_segments;
            std::stringstream http_segment_stream(request_header_lines[0]);
            std::string required_segment;
            while(std::getline(http_segment_stream,required_segment,' '))
                http_required_segments.push_back(required_segment);
            
            if(http_required_segments.size() != 3)
                throw std::runtime_error("Failed to parse HTTP header required info !. Aborting operation !");
            
            Generic::HTTP_REQUEST_METHOD request_method = 
                            Generic::request_method_from_string(http_required_segments[0]);
                            
            const std::string &resource = http_required_segments[1];
                            
            Generic::HTTP_VERSION http_version = Generic::http_version_from_string(http_required_segments[2]);
            
            std::map<std::string,HTTP_HEADER> headers;
            if(request_header_lines.size() > 1)
            {
                
                std::vector<std::string>::iterator request_line_pointer = std::next(request_header_lines.begin() );
                while(request_line_pointer != request_header_lines.end())
                {
                    const HTTP_HEADER header = HTTP_HEADER::deserialize(*request_line_pointer);
                    headers.insert(std::make_pair(header.get_key(),header));
                    request_line_pointer++;
                }
            }

            return HTTP_REQUEST(request_method,resource,headers,http_version);
            
        }
    
    
}
